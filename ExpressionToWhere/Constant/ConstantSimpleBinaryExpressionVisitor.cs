﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ExpressionToWhere
{
    /// <summary>
    /// worker.Id == 1+id中用来计算1+id的值
    /// </summary>
    internal class ConstantSimpleBinaryExpressionVisitor : ConstantExpressionVisitor
    {
        protected override Expression VisitBinary(BinaryExpression node)
        {
            Regex regNumber = new System.Text.RegularExpressions.Regex(@"^[-]?\d+(\.\d+)?$");
            object left = ExpressionProcesser.GetConstant(node.Left);
            object right = ExpressionProcesser.GetConstant(node.Right);
            ExpressionType type = node.NodeType;
            //(int)(2m/3)==1但(int)(2.0/3)==0，所以decimal类型要单独处理
            if (type == ExpressionType.Add)
            {
                if (regNumber.IsMatch(left.ToString()) && regNumber.IsMatch(right.ToString()) && (left is decimal || right is decimal))
                {
                    Value = Convert.ToDecimal(left) + Convert.ToDecimal(right);
                }
                else if (regNumber.IsMatch(left.ToString()) && regNumber.IsMatch(right.ToString()))
                {
                    Value = Convert.ToDouble(left) + Convert.ToDouble(right);
                }
                else
                {
                    Value = left.ToString() + right.ToString();
                }
            }
            else if (type == ExpressionType.Subtract)
            {
                if(left is decimal || right is decimal)
                {
                    Value = Convert.ToDecimal(left) - Convert.ToDecimal(right);
                }
                else
                {
                    Value = Convert.ToDouble(left) - Convert.ToDouble(right);
                }
                
            }
            else if (type == ExpressionType.Multiply)
            {
                if (left is decimal || right is decimal)
                {
                    Value = Convert.ToDecimal(left) * Convert.ToDecimal(right);
                }
                else
                {
                    Value = Convert.ToDouble(left) * Convert.ToDouble(right);
                }
            }
            else if (type == ExpressionType.Divide)
            {
                if (left is decimal || right is decimal)
                {
                    Value = Convert.ToDecimal(left) / Convert.ToDecimal(right);
                }
                else
                {
                    Value = Convert.ToDouble(left) / Convert.ToDouble(right);
                }
            }
            else if (type == ExpressionType.Modulo)
            {
                if (left is decimal || right is decimal)
                {
                    Value = Convert.ToDecimal(left) % Convert.ToDecimal(right);
                }
                else
                {
                    Value = Convert.ToDouble(left) % Convert.ToDouble(right);
                }
            }
            return node;
        }
    }
}
