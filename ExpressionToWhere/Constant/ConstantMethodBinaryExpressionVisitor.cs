﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionToWhere
{
    /// <summary>
    /// worker.Name == name.Substring(0, 3) + "999"中用来计算name.Substring(0, 3) + "999"的值
    /// </summary>
    internal class ConstantMethodBinaryExpressionVisitor: ConstantExpressionVisitor
    {
        protected override Expression VisitBinary(BinaryExpression node)
        {
            object left = ExpressionProcesser.GetConstant(node.Left);
            object right = ExpressionProcesser.GetConstant(node.Right);
            MethodInfo methodInfo = node.Method;
            if (methodInfo.IsStatic)
            {
                Value = methodInfo.Invoke(null, new object[] { left, right });
            }
            else
            {
                Value = methodInfo.Invoke(left, new object[] { right });
            }

            return node;
        }
    }
}
