﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionToWhere
{
    /// <summary>
    /// worker.Name.In(new List<string>() { "aaa", "bbb", "ccc" })中用来解析和处理List中的元素
    /// </summary>
    internal class ConstantListInitExpressionVisitor : ConstantExpressionVisitor
    {
        protected override Expression VisitListInit(ListInitExpression node)
        {
            List<string> list = new List<string>();
            for (int i = 0; i < node.Initializers.Count; i++)
            {
                object itemValue = ExpressionProcesser.GetConstant(node.Initializers[i].Arguments[0]);
                if (itemValue != null)
                {
                    list.Add(itemValue.ToString());
                }                
            }
            Value = list;
            return node;
        }
    }
}
