﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionToWhere
{
    /// <summary>
    /// worker.Id==1中用来提取等号右边的值
    /// </summary>
    internal class ConstantExpressionVisitor: ExpressionVisitor
    {
        public object Value { get; set; }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            this.Value = node.Value;
            return node;
        }
    }
}
