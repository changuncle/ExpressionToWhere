﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionToWhere
{
    /// <summary>
    /// worker.Name.In(new List<string>() { "333" + new Config().InstanceGetName(name) })中用来执行new Config()
    /// </summary>
    internal class ConstantNewExpressionVisitor : ConstantExpressionVisitor
    {
        protected override Expression VisitNew(NewExpression node)
        {
            Type type = node.Type;
            string typeFullName = type.FullName;
            Value = type.Assembly.CreateInstance(typeFullName);
            return node;
        }
    }
}
