﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionToWhere
{
    /// <summary>
    /// worker.Name.In(new List<string>() { })中用来处理new List<string>() { }
    /// </summary>
    internal class ConstantMemberInitExpressionVisitor : ConstantExpressionVisitor
    {
        protected override Expression VisitMemberInit(MemberInitExpression node)
        {
            Value = ExpressionProcesser.GetConstant(node.NewExpression);
            return node;
        }
    }
}
