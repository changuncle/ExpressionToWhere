﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionToWhere
{
    /// <summary>
    /// 
    /// </summary>
    internal class ConstantConditionalExpressionVisitor : ConstantExpressionVisitor
    {
        protected override Expression VisitConditional(ConditionalExpression node)
        {
            bool condition = (bool)ExpressionProcesser.GetConstant(node.Test);
            if (condition)
            {
                Value = ExpressionProcesser.GetConstant(node.IfTrue);
            }
            else
            {
                Value = ExpressionProcesser.GetConstant(node.IfFalse);
            }
            return node;
        }
    }
}
