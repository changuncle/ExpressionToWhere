﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionToWhere
{
    internal class MemberInfoProcesser
    {
        public static string GetColumnName(MemberInfo memberInfo)
        {
            return memberInfo.Name;
        }

        public static string GetParameterName(MemberInfo memberInfo)
        {
            return memberInfo.Name;
        }
    }
}
