﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionToWhere
{
    internal class Config
    {
        /// <summary>
        /// in参数化查询参数名的前缀
        /// </summary>
        public static string InConditionPrefix = "InCondition";
    }
}
