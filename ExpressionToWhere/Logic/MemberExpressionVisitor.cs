﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionToWhere
{
    /// <summary>
    /// worker.Id==1中用来提取属性名Id
    /// </summary>
    internal class MemberExpressionVisitor :BaseExpressionVisitor
    {
        public MemberInfo MemberInfo { get; private set; }

        protected override Expression VisitMember(MemberExpression node)
        {
            MemberInfo memberInfo = node.Member;
            sb.Append(MemberInfoProcesser.GetColumnName(memberInfo));
            MemberInfo = memberInfo;
            return node;
        }
    }
}
