﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionToWhere
{
    /// <summary>
    /// 
    /// </summary>
    internal class BooleanMemberExpressionVisitor : MemberExpressionVisitor
    {
        protected virtual bool GetConstant()
        {
            return true;
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            base.VisitMember(node);
            string fieldName = GetResult().ToString();
            sb.Clear();

            string parameterName = ExpressionProcesser.GetPatameterName(MemberInfo);
            sb.Append($"{fieldName} = @{parameterName}");
            ExpressionProcesser.Parameters.Add($"@{parameterName}", GetConstant());
            return node;
        }
    }
}
