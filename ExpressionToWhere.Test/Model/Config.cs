﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionToWhere.Test
{
    public class Config
    {
        public static int Id = 5;

        public static int Age = 12;

        public static string Name = "Guo";

        public static string GetName(string name)
        {
            return name + "Static";
        }

        public string InstanceGetName(string name)
        {
            return name + "Instance";
        }
    }
}
