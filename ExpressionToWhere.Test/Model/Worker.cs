﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionToWhere.Test
{
    public class Worker
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public int? Height1 { get; set; }
        public float? Height2 { get; set; }
        public double? Height3 { get; set; }
        public decimal? Height4 { get; set; }
        public string Address { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
